import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getTestTag() {
    return element(by.css('.e2e-tag')).getAttribute('id');
  }

  searchForCategoryAndClick() {
    // We now that after searching for "lorem", the first suggested result will be a category
    element(by.css('.searchbox-input')).sendKeys('lorem');
    element(by.css('.dropdown-item')).click();
  }

  clickOnTheFirstEvent() {
    element(by.css('.event-title')).click();
  }

  clickOnLoginButton() {
    element(by.id('Login')).click();
  }
}
