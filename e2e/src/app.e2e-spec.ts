import { AppPage } from './app.po';

const tags = {
  HOME: 'home',
  CATEGORY: 'category',
  ACCOUNT: 'account',
  EVENT: 'event'
};

describe('workspace-project App', () => {
  let page: AppPage;

  page = new AppPage();
  page.navigateTo();

  it('Should start from the Home page', () => {
    expect(page.getTestTag()).toEqual(tags.HOME);
  });

  it('Should navigate to a category page after looking for one in the searchbox', () => {
    page.searchForCategoryAndClick();
    expect(page.getTestTag()).toEqual(tags.CATEGORY);
  });

  it('In a category page, it should go to event page after clicking in an event', () => {
    page.clickOnTheFirstEvent();
    expect(page.getTestTag()).toEqual(tags.EVENT);
  });

  it('On any page, it should go to account after clicking in the login button', () => {
    page.clickOnLoginButton();
    expect(page.getTestTag()).toEqual(tags.ACCOUNT);
  });
});
