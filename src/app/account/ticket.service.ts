/*********************************************************
 * Service to make tickets related request to the API.
 *********************************************************/
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '../shared/base.service';

/**
 * Interface with the structure which whill have the tickets retrieved from the API
 */
export interface ITicket {
  sellerId: number;
  eventId: number;
  id: number;
  quantity: number;
  unit_price: number;
  status: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class TicketService extends BaseService {
  constructor(protected http: HttpClient) {
    super();
  }

  /**
   * Retrieves tickets belonging to a specific seller
   * @param sellerId sellers identificator
   * @return Observable with the ticket-list
   */
  getTicketsFromSeller(sellerId: string): Observable<ITicket[]> {
    return this.requestFromApi<ITicket[]>(`tickets?sellerId=${sellerId}`);
  }

  /**
   * Retrieves tickets which are available related to an event.
   * @param eventId event identificator
   * @return Observable with the ticket-list
   */
  getAvailableTicketsFromEvent(eventId: string): Observable<ITicket[]> {
    return this.requestFromApi<ITicket>(`tickets?eventId=${eventId}&status=true`);
  }
}
