import { DatePipe, TitleCasePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { AccountComponent } from './account.component';

@Component({ selector: 'app-searchbox', template: '' })
class SearchboxComponent {}

describe('AccountComponent', () => {
  let component: AccountComponent;
  let fixture: ComponentFixture<AccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountComponent, SearchboxComponent],
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [DatePipe, TitleCasePipe]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountComponent);
    component = fixture.componentInstance;

    component.tickets = [
      { sellerId: 1, eventId: 1, id: 1, quantity: 1, unit_price: 1, status: true }
    ];

    component.events = {
      1: {
        categoryId: 1,
        id: 1,
        title: 'MockedEvent',
        description: '',
        date: new Date(),
        imageUrl: '',
        thumbnailImageUrl: '',
        city: '',
        country: '',
        venueName: ''
      }
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Cloning', () => {
    beforeEach(() => {
      const sourceTicket = component.tickets[0];
      component.cloneTicket(sourceTicket);
      fixture.detectChanges();
    });

    it('should update the list', () => {
      expect(component.tickets.length).toBe(2);
    });

    it('Should update the screen list', () => {
      const de = fixture.debugElement.queryAll(By.css('.ticket-event'));
      expect(de.length).toBe(2);
    });
  });

  describe('Removing', () => {
    beforeEach(() => {
      const sourceTicket = component.tickets[0];
      component.removeTicket(sourceTicket);
      fixture.detectChanges();
    });

    it('should update the list', () => {
      expect(component.tickets.length).toBe(0);
    });

    it('Should update the screen list', () => {
      const de = fixture.debugElement.queryAll(By.css('.ticket-event'));
      expect(de.length).toBe(0);
    });
  });

  describe('Changing status', () => {
    it('Changes the status a given ticket', () => {
      const sourceTicket = component.tickets[0];
      const initialStatus = sourceTicket.status;
      component.changeStatus(sourceTicket);
      expect(sourceTicket.status).toBe(!initialStatus);
    });

    it('Should add .disabled class to disabled tickets', () => {
      component.tickets[0].status = false;
      fixture.detectChanges();
      const de = fixture.debugElement.query(By.css('.ticket-event'));
      expect(de.classes['disabled']).toBeTruthy();
    });
  });
});
