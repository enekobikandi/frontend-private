/******************************************************
 * Component containing the logic for the account page
 ******************************************************/
import { DatePipe, TitleCasePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { zip } from 'rxjs';
import { EventService, IEventDictionary } from '../event/event.service';
import { createEventDictionary } from '../shared/helpers';
import { ITicket, TicketService } from './ticket.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html'
})
export class AccountComponent implements OnInit {
  tickets: ITicket[];
  events: IEventDictionary;

  constructor(
    private ticketService: TicketService,
    private eventService: EventService,
    private datePipe: DatePipe,
    private titleCasePipe: TitleCasePipe
  ) {}

  /**
   * As the Readme file says, we'll retrieve the tickets from the seller with the id=1.
   * Also, it gets the events to display the details in the screen, as the tickets do not have
   * any details about the events.
   */
  ngOnInit() {
    zip(
      this.ticketService.getTicketsFromSeller('1'),
      this.eventService.getEvents()
    ).subscribe(([tickets, events]) => {
      this.tickets = tickets;
      this.events = createEventDictionary(events);
    });
  }

  /**
   * Clones the ticket and pushes it to the list.  We won't worry about setting
   * the new Id, as this is a POC ,in a real scenario the API should be the
   * responsible of doing it. Also, As it is not specified, we'll insert
   * the new ticket next to the original one
   * @param ticket ticket to clone
   */
  cloneTicket(ticket: ITicket) {
    const index = this.tickets.indexOf(ticket);

    const newTicket = { ...ticket };

    this.tickets.splice(index + 1, 0, newTicket);
  }

  /**
   * Removes the given tickets from the list.
   * @param ticket ticket to remove
   */
  removeTicket(ticket: ITicket) {
    const index = this.tickets.indexOf(ticket);
    this.tickets.splice(index, 1);
  }

  /**
   * Switches the tickets status from false to true or viceversa.
   * @param ticket ticket to change its status
   */
  changeStatus(ticket: ITicket) {
    ticket.status = !ticket.status;
  }

  /**
   * Transforms an events title to a more readable Title Case format.
   * @param eventId The events identificator to transform its title
   */
  getEventName(eventId: number) {
    return this.titleCasePipe.transform(this.events[eventId].title);
  }

  /**
   * Gets an events date and formats it.
   * @param eventId The events identificator to get its date
   * @return formatted date
   */
  getEventDate(eventId: number) {
    return this.datePipe.transform(this.events[eventId].date, 'yyyy-MM-dd HH:mm');
  }

  /**
   * Returns an events location based in its city and country.
   * @param eventId The events identificator to get its location
   * @return location
   */
  getEventLocation(eventId: number) {
    return `${this.events[eventId].city}, ${this.events[eventId].country}`;
  }

  /**
   * Returns an events thumbnail image
   * @param eventId The events identificator to get its image url
   * @return thubmnail image url
   */
  getEventImage(eventId: number) {
    return this.events[eventId].thumbnailImageUrl;
  }
}
