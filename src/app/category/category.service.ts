/*********************************************************
 * Service to make categories related request to the API.
 *********************************************************/
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '../shared/base.service';

/**
 * Interface with the structure which will have a category retrieved from the API
 */
export interface ICategory {
  id: number;
  name: string;
  description: string;
}

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends BaseService {
  constructor(protected http: HttpClient) {
    super();
  }
  /**
   * Gets all the categories from the API
   * @return Observable wich the category-list
   */
  getCategories(): Observable<ICategory[]> {
    return this.requestFromApi<ICategory>(`categories`);
  }
}
