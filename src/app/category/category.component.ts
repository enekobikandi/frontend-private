/******************************************************
 * Component containing the logic for the category page
 ******************************************************/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventService, IEvent } from '../event/event.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html'
})
export class CategoryComponent implements OnInit {
  events: IEvent[];

  constructor(private route: ActivatedRoute, private eventService: EventService) {}

  /**
   * Method to be called on init time. Gets the id queryparam from the url and
   * requests the events related to the category that has that identificator.
   */
  ngOnInit() {
    const categoryId = this.route.snapshot.paramMap.get('id');
    this.eventService
      .getEventsFromCategory(categoryId)
      .subscribe(events => (this.events = events));
  }
}
