import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CategoryComponent } from './category.component';

import { of } from 'rxjs';
import { EventService, IEvent } from '../event/event.service';

describe('CategoryComponent', () => {
  @Component({ selector: 'app-searchbox', template: '' })
  class SearchboxComponent {}

  let component: CategoryComponent;
  let fixture: ComponentFixture<CategoryComponent>;

  const mockedEvent: IEvent = {
    categoryId: 1,
    id: 1,
    title: 'MockedEvent',
    description: '',
    date: new Date(),
    imageUrl: '',
    thumbnailImageUrl: '',
    city: '',
    country: '',
    venueName: ''
  };
  class EventServiceMock {
    getEventsFromCategory() {
      return of([mockedEvent]);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CategoryComponent, SearchboxComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule],
      providers: [{ provide: EventService, useClass: EventServiceMock }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get events from the api', () => {
    expect(component.events.length).toBe(1);
  });
});
