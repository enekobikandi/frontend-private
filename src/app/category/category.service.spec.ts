import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { CategoryService } from './category.service';

describe('CategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [CategoryService]
    });
  });

  it(
    'should be created',
    inject([CategoryService], (service: CategoryService) => {
      expect(service).toBeTruthy();
    })
  );
});
