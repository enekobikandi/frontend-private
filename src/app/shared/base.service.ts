/*********************************************************
 * Base Service with generyc typed request to the API aimed
 * to be extended for the rest of the "specific" services.
 *********************************************************/
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

export class BaseService {
  protected http: HttpClient;

  /**
   * It will do a request to an API endpoint depending on the given path.
   * It will log an error if the request goes wrong.
   * @param path
   * @return Observable with the response
   */
  protected requestFromApi<T>(path: string): Observable<any> {
    return this.http
      .get<T[]>(`${environment.apiurl}/${path}`)
      .pipe(catchError(this.handleError(path, [])));
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  protected handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
