/***************************************
 * Some helper methods
 ****************************************/
import { IEvent } from '../event/event.service';

export function createDictionaryFromList<T>(list: T[], keyProp: string): {} {
  return list.reduce((currentDictionary: {}, item: T) => {
    const id = item[keyProp];
    currentDictionary[id] = item;
    return currentDictionary;
  }, {});
}

export function createEventDictionary(eventList: IEvent[]) {
  return createDictionaryFromList<IEvent>(eventList, 'id');
}
