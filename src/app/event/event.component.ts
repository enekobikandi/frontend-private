/******************************************************
 * Component containing the logic for the event page
 ******************************************************/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { zip } from 'rxjs';
import { ITicket, TicketService } from '../account/ticket.service';
import { EventService } from '../event/event.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html'
})
export class EventComponent implements OnInit {
  tickets: ITicket[];
  eventTitle: string;

  constructor(
    private route: ActivatedRoute,
    private ticketService: TicketService,
    private eventService: EventService
  ) {}

  /**
   * Method to be called on init time. Gets the id queryparam from the url and
   * requests the tickets related to the event that has that identificator.
   * Also, gets the event form the API to show its title in the screen.
   */
  ngOnInit() {
    const eventId = this.route.snapshot.paramMap.get('id');

    zip(
      this.ticketService.getAvailableTicketsFromEvent(eventId),
      this.eventService.getEventById(eventId)
    ).subscribe(([tickets, event]) => {
      this.tickets = tickets;
      this.eventTitle = event.title;
    });
  }
}
