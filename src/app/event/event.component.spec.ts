import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ITicket, TicketService } from '../account/ticket.service';
import { EventComponent } from './event.component';
import { EventService } from './event.service';

describe('EventComponent', () => {
  let component: EventComponent;
  let fixture: ComponentFixture<EventComponent>;

  const mockedTicket: ITicket = {
    sellerId: 1,
    eventId: 1,
    id: 1,
    quantity: 1,
    unit_price: 1,
    status: true
  };
  const mockedEvent = {
    title: 'Mocked Event'
  };
  class TicketServiceMock {
    getAvailableTicketsFromEvent() {
      return of([mockedTicket]);
    }
  }

  class EventServiceMock {
    getEventById() {
      return of(mockedEvent);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EventComponent],
      imports: [RouterTestingModule, HttpClientModule, HttpClientTestingModule],
      providers: [
        { provide: TicketService, useClass: TicketServiceMock },
        { provide: EventService, useClass: EventServiceMock }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get tickets from the api', () => {
    expect(component.tickets.length).toBe(1);
    expect(component.tickets[0]).toBe(mockedTicket);
    expect(component.eventTitle).toBe(mockedEvent.title);
  });
});
