/*********************************************************
 * Service to make event related request to the API.
 *********************************************************/
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '../shared/base.service';

/**
 * Interface with the structure which whill have an event retrieved from the API
 */
export interface IEvent {
  categoryId: number;
  id: number;
  title: string;
  description: string;
  date: Date;
  imageUrl: string;
  thumbnailImageUrl: string;
  city: string;
  country: string;
  venueName: string;
}

// Definition of the EventDictionary
export interface IEventDictionary {
  [id: number]: IEvent;
}

@Injectable({
  providedIn: 'root'
})
export class EventService extends BaseService {
  constructor(protected http: HttpClient) {
    super();
  }

  /**
   * Gets all the events
   * @return Observavle with the event-list
   */
  getEvents(): Observable<IEvent[]> {
    return this.requestFromApi<IEvent[]>('events');
  }

  /**
   * Gets the details related to an specific event
   * @param eventId event identificator
   * @return Observavle with the event-details
   */
  getEventById(eventId: string): Observable<IEvent> {
    return this.requestFromApi<IEvent>(`events/${eventId}`);
  }

  /**
   * Gets the events related to an specific category
   * @param categoryId category identificator
   * @return Observavle with the event-list
   */
  getEventsFromCategory(categoryId: string): Observable<IEvent[]> {
    return this.requestFromApi<IEvent>(`categories/${categoryId}/events`);
  }
}
