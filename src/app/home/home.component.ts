/******************************************************
 * Component containing the logic for the home page
 ******************************************************/
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
