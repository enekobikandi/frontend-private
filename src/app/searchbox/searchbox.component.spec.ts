import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { CategoryService, ICategory } from '../category/category.service';
import { EventService, IEvent } from '../event/event.service';
import { SearchboxComponent } from './searchbox.component';

class MockedComponent {}

describe('SearchboxComponent', () => {
  let component: SearchboxComponent;
  let fixture: ComponentFixture<SearchboxComponent>;
  let router: Router;

  const mockedRoutes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: MockedComponent },
    { path: 'account', component: MockedComponent },
    { path: 'category/:id', component: MockedComponent },
    { path: 'event/:id', component: MockedComponent }
  ];
  const mockedCategory: ICategory = {
    id: 1,
    name: 'MockedCategory',
    description: ''
  };

  const mockedEvent: IEvent = {
    categoryId: 1,
    id: 1,
    title: 'MockedEvent',
    description: '',
    date: new Date(),
    imageUrl: '',
    thumbnailImageUrl: '',
    city: '',
    country: '',
    venueName: ''
  };
  class EventServiceMock {
    getEvents() {
      return of([mockedEvent]);
    }
  }

  class CategoryServiceMock {
    getCategories() {
      return of([mockedCategory]);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchboxComponent],
      imports: [
        NgbTypeaheadModule.forRoot(),
        HttpClientModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(mockedRoutes)
      ],
      providers: [
        DatePipe,
        { provide: EventService, useClass: EventServiceMock },
        { provide: CategoryService, useClass: CategoryServiceMock }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    fixture = TestBed.createComponent(SearchboxComponent);
    component = fixture.componentInstance;
    router.initialNavigation();
    fixture.detectChanges();
  });
  afterEach(() => {
    SearchboxComponent.items = [];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get cagegories and events from the api', () => {
    expect(SearchboxComponent.items.length).toBe(2);
  });

  it('should suggest the a item from the list depending on the searchTerm', done => {
    component.search(of('catego')).subscribe(items => {
      expect(items.length).toBe(1);
      expect(items[0].name).toBe('MockedCategory');
      done();
    });
  });

  it('should go to category page after selecting a category', done => {
    component.goTo(SearchboxComponent.items[0]).then(() => {
      expect(router.url).toBe('/category/1');
      done();
    });
  });

  it('should go to category page after searching for a category', done => {
    component.findAndGoTo('catego').then(() => {
      expect(router.url).toBe('/category/1');
      done();
    });
  });

  it('should go to event page after selecting a event', done => {
    component.goTo(SearchboxComponent.items[1]).then(() => {
      expect(router.url).toBe('/event/1');
      done();
    });
  });

  it('should go to event page after searching for a event', done => {
    component.findAndGoTo('event').then(() => {
      expect(router.url).toBe('/event/1');
      done();
    });
  });
});
