/**********************************************************************
 * Component containing the logic for the reusable SearchBox component
 *********************************************************************/
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, zip } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { CategoryService } from '../category/category.service';
import { EventService } from '../event/event.service';

/**
 * Interface with the structure related with the objects that will handle the
 * searchbox. Containing the name and description of the source object, and
 * an url to go to the category/event pages.
 */
interface ISearcheable {
  url: string;
  name: string;
  description: string | Date;
}
@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html'
})
export class SearchboxComponent implements OnInit {
  // Make it static in order to make accesible from the search() function.
  static items: ISearcheable[] = [];

  constructor(
    private categoryService: CategoryService,
    private eventService: EventService,
    private router: Router,
    private datePipe: DatePipe
  ) {}

  /**
   * Method to be called on init time. Requests all the categories and events,
   * initializing a list with their information. The list will be used to give
   * suggestions to the user depending on the user input.
   */
  ngOnInit() {
    zip(
      this.categoryService.getCategories(),
      this.eventService.getEvents()
    ).subscribe(([categories, events]) => {
      categories.forEach(({ id, name, description }) =>
        SearchboxComponent.items.push({
          url: `category/${id}`,
          name,
          description
        })
      );
      events.forEach(({ id, title, date }) =>
        SearchboxComponent.items.push({
          url: `event/${id}`,
          name: title,
          description: this.datePipe.transform(date, 'yyyy-MM-dd HH:mm')
        })
      );
    });
  }

  /**
   * It will replace the searchTerm in the elements complete name with an HTML
   * code to show the user the bold-styled term within the name.
   * @param term searchterm
   * @param name elements complete name
   */
  strongifyTerm(term: string, name: string) {
    return name.replace(term, '<strong>' + term + '</strong>');
  }

  /**
   * Will filter the item-list for item with names containing the name.
   * After a search it will wait a debounceTime as a safety method if the input
   * changes too frequently.
   * @param text$ Observable with the input value
   */
  search(text$: Observable<string>) {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(
        term =>
          term.length < 2
            ? []
            : SearchboxComponent.items
                .filter(
                  item => item.name.toLowerCase().indexOf(term.toLowerCase()) > -1
                )
                .slice(0, 10)
      )
    );
  }

  /**
   * Will get the searched item and return a readable name.
   * @param item
   * @return the items name
   */
  formatter(item: ISearcheable): string {
    return item.name;
  }

  /**
   * Navigate to the items page. It can be a category or a event.
   * @param item
   * @return A promise that will be resolved when the url is valid.
   */
  goTo(item: ISearcheable): Promise<boolean> {
    return this.router.navigate([item.url]);
  }

  /**
   * Will go to page related to the first term-matching element, if any.
   * Else, will show an alert.
   * @param searchTerm
   */
  findAndGoTo(searchTerm: string): Promise<boolean> {
    const firstFound = SearchboxComponent.items.find(item =>
      item.name.toLowerCase().includes(searchTerm.toLowerCase())
    );
    if (firstFound) {
      return this.goTo(firstFound);
    } else {
      alert('No category found');
      return Promise.reject(false);
    }
  }
}
