#Stubhub Frontend Test

#### _by Eneko Bikandi_

In this codebase there is an implementation of the mocks provided by the designers (explained in the _Exercise.md_ file).

The exercise should be explained in three points:

* **Disclaimer**. Some thoughts from the developer.
* **Approach**. To explain the approach taken and its logic.
* **How to Run**. A little guide to run the app.

##Disclaimer
Time has been a key factor when doing this exercise. I have been working and studying at the time I have been requested to resolve it, so I think is worth to spend some lines trying to explain myself. Also, despite fulfilling all the requisites, there is always something to improve, so I believe it is worth to list some ideas I wished to implement but I did not have the time.

###Why this approach?

* **Angular vs React vs Vue**. This decision has been one of the main ones, because it conditions the rest of the work. Personally, I enjoy working with the three. My first option has been Vue, but despite of having a good documentation, a handy CLI and being the lightest of the three, personally I do not like its _single-file-component_ philosophy, because I preffer not having all the HTML, styles and component logic on a single file per component. The second option has been React, but its CLI relies on a quite simple configuration, and introducing some other technologies (like the CSS preprocessor) would ask a manual configuration, spending some valuable time. So, this lets me Angular. I avoided this option at firsts, because it is the most heavy of the three and it has its own way of configuration (modules, services, ...). But as it has a very good documentation and its awesome CLI gives some options to automate tasks and switch between tools, I decided to go for [Angular v6](https://angular.io/). Actually, once the configuration phase is done, it's pretty straightforward and easy to start writing code. **I would like to remark that although the rquestes version in the *Execise.md* is AngularJs, I understood the it was about "the new" Angular (note that AngularJs is the oldie v1 of Angular). I chose to use the latest version in order to use the most modern tool.**
* **Trimmed texts** Some texts are quite long so they have been trimmed and a simple tooltip will be shown when they are hovered. (eg. eventName in category page).
* **Why not so much e2e testing?** Angular TestBed configuration sets a good unit testing scenario, able to test component logic and DOM behaviour. So it has been decided to rely on unit testing and only use e2e testing to test the flow aplication and API integration. Actually, Angular suggests this in its [docu](https://angular.io/guide/testing#why-not-rely-on-e2e-tests-of-dom-integration).

###Future ideas
These are some ideas that have been put off:

* **Check for unsaved changes.** As the account page has a form, it would be a mandatory feature in a real application to check for unsaved changes when leaving the page and ask the user if he/she really wants to leave.
* **Continue with styles refactoring.** The styles have been separated in files and they are preprocessed with sass. Anyway, a good idea would be to introduce linting rules (e.g with sass-lint) and continue the refactor depending on them.
* **Hide params in url.** Some identifiers are sent in the url. It would be a good idea to hide or obfuscate them.
* **Img zoom on hover.** A cool feature on the Stubhub page is the little zoom effect that images have on hover, so this would a "good to have".
* **Better mobile friendly.** It would be nice to style the web to be more mobile friendly and adapt it to mobile screen sizes. **Some tewaks have been made**, like hiding searchbox buttons text in modile screens but the styles can be improved anyway.
* **Header and footer stuck in upper/lower sides.** The Header and the footer go up and down depending on the loaded content. So it would be good to, at least, stick them to the upper/lower sides if the content is empty.
* **Loading spinner**. A good UX feature would be to show a loading spinner and hide the content until it is fully loaded.
* **Message for empty content.** Also, displaying a "_No content to show_" message when the content are empty would be nicer than displaying nothing when no there is no data to show.
* **Better search-result page.** Currently after clicking the "_search_" button, the user will go to the first matches details page (if any), or the web will display an alert if there is no match. A good to have would be to show a page displaying all the results for the input term.
* **CI pipeline.** Bitbucket offers setting a CI pipeline to do some automated task (e.g run the tests whenever a _pull request_ is done), but I would need to ask for admin rights in the repo to configure that.
* **Some TODOs in the code.** A couple future work and small tweaks documented in the code.

##The Approach
As I said, the web has been developed with Angular. It has its own particularities related to configuration, development and execution phases, but once the learning curve is overcomed, it is quite simple to go developing. Also, its awesome CLI has some commands to create "_pieces_" easily and attach them to the application. Furthermore,  it includes linters, options to introduce other tools like CSS preprocessors, etc. This has allowed to spend less time in configuration time and focus more in development, so these are the main reasons why I choose this tool.

###Folder structure
The webs main are in the _src_ folder while the the rest are configuration and running related ones. Each file has its own _spec_ file to gather all its tests. These files are distributed in this way:


![folders](./_readme_img/folders.png)

* **app.** It contains all the bussiness logic (components, services, ...). Remark the _app.module_, which is the responsible of gathering all the parts. - **Component folder (eg. account).** Each component has its own folder. Here there will be its _html_ page and the _component_ file with all its logic. Also some services have been created and put inside its most related component. **Services are the ones responsible of comunicating with the API**. Each .component will have its own _.spec_ file with the tests. - **shared.** This folder will have the logic shared between components. As a _base-service_ aimed to be extended of the rest of the services and the _helpers_ file This file was going to have functions shared along components (e.g to create dictionaries from lists). Anyway, at the end, only one component has used this function but it has been decided to let the function in the _helpers_ file in case it would be used in the future by other different component.
* **assets.** Folder to put the static content (images, etc.).
* **environments** Here ww have the files specifying the enviroment variables. These are useful when you have more than one environment (imagine having one for CI and a second one for production)
* **sass.** The style sheets folder. We will have one per style group (events, tickets, etc.), another one named *shared* for shared styles and a _main_ file where all will be imported. These files will be preprocessed by Angular using _sass_ in compilation time.

###Two special mentions
There are two things in this codebase I would like to mention:

* **SearhBoxComponent.** This is a reusable component, used in three pages (home, category and account). Some things about this component: 
	- It uses the [ngbtypeahed](https://ng-bootstrap.github.io/#/components/typeahead/examples) library to give the typeahed functionality to the searchbox. This is based in the [twitter typeahead](https://twitter.github.io/typeahead.js/) library, which I believe is the one used byu the stubhub page. 
	- Despite the Exercise.md says that only categories are displayed in the searchbox, after contacting my contact in the Sutbhub hiring process, she told me that **the ideal scenario would be to get the categories and events, order them by priorities and in give suggestions on both.** So, as the Stubhub pages searchbox retrieves the categories and events and the suggestion list given is sorted giving categories at first and then events, similar functionality has been developed for this searchbox also. 
	- The *Exercise.md* defines the behaviour of the searchbox in the _home_ and the _category_ page. Anyway, there is a searchbox in the _account_ page too but there is no behaviour specified, so the same behaviour has been mantained (searching categories and events) for the _account_ pages searchbox too.
* **BaseService**. Instead of developing a single service to make all the requests to the API, a more modular approach has been taken. There will be calls related to categories, events and tickets, so there will be a service for each "topic" to gather all the requests related to it. These would involve some duplicated code about making request (the only changing things would be the endpoint path and the return type) and error-handling. So we made this generic type service to be extended by the other services and that way give them request-doing and error-handling functionalities. All that other services should worry about is initializing the *http* object to do the request (**which is easily made by _dependency injection_)** and specifying the endpoint path and the requested object type.

###Tests (\*.spec.\* files)
After fulfilling the min functionalities, focus has been put on test, despite not being mentioned in the _Exercise.md_ file. As I preffer more _common fashioned_ tests and other frameworks (Angular uses Karma + Jasmine by default and I preffer using Jest), [Angular TestBed](https://angular.io/api/core/testing/TestBed) offers a good scenario to test both, logic and DOM, after its properly configured.

There are two types of tests:

* **Unit tests.** These are the most extended ones in the app. They test each component individually, testing its logic and DOM without taking into consideration other components, which are mocked if they are needed.
* **e2e tests.** These tests are aimed to act as integration test between parts (components, services, API, etc.). They test a flow through the different pages of the web a commmon user would do, including real requests to the API. The real flow simmulation would be:
	* A user starts from the Home page and searches for a category/event.
	* After clicked in the suggestions, the user will go to the details page:
		* If it is a category page, related events will be show. There, if it clickes an event, he/she will go to the event page to see the related tickets.
		* If the user has clicked an event, he/she will go to the event page where the tickets are listed.
	* Anytime, if the _Login_ button is clicked, it should go to the account _page_.

Both types of tests would run in a [headless Chrome](https://developers.google.com/web/updates/2017/04/headless-chrome), that is, they will be run in a Chrome engine without the need of having a Chrome browser opened.
##How to Run
The only two things that you should install manually are [node (the LTS version is recommended)](https://nodejs.org/es/) and the [Angular CLI](https://angular.io/guide/quickstart#quickstart). After installing both and downloading/cloning the repo, access the repos folder from a terminal/command line and run `npm i` (_npm_ comes with node) to install all the dependencies. After this step, you should be able to run the app and its tests. Here are some handy commmands:

Command | Description
-------|------------------------
`npm install -g @angular/cli` | Install the Angular CLI (after installing *node*).
`npm i` | Install the dependicies.
`ng build` | Builds the app. The bundle will be put in the _dist_ folder in the root level. As a good practice, you can use the `--prod` arg to build it uglified for production enviroment.
`ng serve -o` | Builds the app and opens it in a browser.
`ng test` | Builds the app and runs the tests in watch mode in a headless browser. You can specify not to watch with `--watch false`. Also, if you want to run the tests in visual Chrome window, you can pass the `--browsers Chrome` arg (suposing that you have Chrome installed in your computer).
`ng e2e` | Builds the app and runs the e2e tests in a headless browser. 

###Demo
So, time for action. Lets run the `ng serve -o` command and lets do a walk through the different pages of the app. A natural flow would be:
`home --> category --> event --> account`

That way, we can test all the screens.
####1-Home
![home-page](./_readme_img/home.png)
This is the initial page. After runnning the `ng serve -o` command this screen should be opened automatically in a browser. You can type anything you want to search in the searchbox and you will receive suggested categories and events based on your search-term. I suggest writting **lorem** as in the screenshot and click on a result (eg. the second one ;-))
####2-Category
![category-page](./_readme_img/category.png)
After clicking in a category, you will go to the category page, where all its events will be shown. I suggest to click on the second one to go to its details.
####3-Event
![event-page](./_readme_img/event.png)
After selecting an event on a category page (or searching for it in the searchbox) you will go to the event details page, where you will be able to buy (not really ;-)) a ticket to go to that event.
####4-Account
![account-page](./_readme_img/account.png)
Independent of which page you are on, if you click on the *Login* button (on the upper right side of the screen), you will go to the user account page. Here you will have a list with all your tickets and you will be able to duplicate, remove and activate or deactivate them. The deactivated ones will be shown with grey background.

 

